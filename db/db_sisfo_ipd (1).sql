-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2020 at 10:32 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sisfo_ipd`
--

-- --------------------------------------------------------

--
-- Table structure for table `desa`
--

CREATE TABLE `desa` (
  `id_desa` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `email` int(50) NOT NULL,
  `id_kecamatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desa`
--

INSERT INTO `desa` (`id_desa`, `nama`, `alamat`, `email`, `id_kecamatan`) VALUES
(1, 'Timbuolo', '-', 1, 1),
(4, 'Panggulo', '-', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dimensi`
--

CREATE TABLE `dimensi` (
  `id_dimensi` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dimensi`
--

INSERT INTO `dimensi` (`id_dimensi`, `nama`) VALUES
(8, 'Pelayanan Dasar'),
(9, 'Kondisi Infrastruktur'),
(11, 'Aksebilitas Transportasi'),
(12, 'Pelayanan Umum'),
(13, 'Penyelenggaraan Pemerintah');

-- --------------------------------------------------------

--
-- Table structure for table `indikator`
--

CREATE TABLE `indikator` (
  `id_indikator` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `id_dimensi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `indikator`
--

INSERT INTO `indikator` (`id_indikator`, `nama`, `id_dimensi`) VALUES
(7, 'Pendidikan', 8),
(8, 'Kesehatan', 8),
(9, 'Infrastruktur Ekonomi', 9),
(10, 'Infrastruktur Energi', 9),
(11, 'Infrastruktur Air Bersih Dan Sanitas', 9),
(12, 'Infrastruktur Komunikasi Dan Informasi', 9),
(13, 'Sarana Transportasi', 11),
(14, 'Aksebilitas Transportasi', 11),
(15, 'Penanganan Kesehatan Masyarakat', 12),
(16, 'Ketersediaan Fasilitas Olahraga', 12),
(17, 'Kemandiriaan', 13);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `nama`) VALUES
(1, 'Botupingge');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `ketersediaan` int(11) NOT NULL,
  `akses` enum('1','0') NOT NULL DEFAULT '0',
  `id_variabel` int(11) NOT NULL,
  `id_desa` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `tahun`, `ketersediaan`, `akses`, `id_variabel`, `id_desa`, `id_user`) VALUES
(2, 2018, 1, '1', 6, 1, NULL),
(3, 2018, 1, '1', 7, 1, NULL),
(4, 2018, 0, '0', 8, 1, NULL),
(5, 2018, 0, '1', 9, 1, NULL),
(6, 2018, 0, '1', 10, 1, NULL),
(7, 2018, 1, '1', 11, 1, NULL),
(8, 2018, 1, '1', 12, 1, NULL),
(9, 2018, 2, '1', 13, 1, NULL),
(10, 2018, 26, '1', 14, 1, NULL),
(11, 2018, 0, '1', 15, 1, NULL),
(12, 2018, 4, '1', 16, 1, NULL),
(13, 2018, 4, '1', 17, 1, NULL),
(16, 2018, 4, '1', 20, 1, NULL),
(18, 2018, 3, '1', 21, 1, NULL),
(19, 2018, 1, '1', 22, 1, NULL),
(21, 2018, 5, '1', 30, 1, NULL),
(22, 2018, 12, '1', 31, 1, NULL),
(23, 2018, 46, '1', 24, 1, NULL),
(25, 2018, 1, '1', 23, 1, NULL),
(26, 2018, 6, '1', 40, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('0','1','2','3') NOT NULL COMMENT '0 = super admin, 1 = kecamatan, 2 = kepala desa, 3 = sekretaris',
  `id_desa` int(11) DEFAULT NULL,
  `id_kecamatan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `username`, `password`, `level`, `id_desa`, `id_kecamatan`) VALUES
(1, 'admin', 'adm@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `variabel`
--

CREATE TABLE `variabel` (
  `id_variabel` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `pembobotan` float NOT NULL,
  `id_indikator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `variabel`
--

INSERT INTO `variabel` (`id_variabel`, `nama`, `pembobotan`, `id_indikator`) VALUES
(6, 'Ketersediaan dan akses ke TK', 0.0227852, 7),
(7, 'Ketersediaan dan akses ke SD', 0.0115521, 7),
(8, 'Ketersediaan dan akses ke SMP', 0.0320783, 7),
(9, 'Ketersediaan dan akses ke SMA', 0.0317407, 7),
(10, 'Ketersediaan dan akses ke Puskesmas', 0.0310473, 8),
(11, 'Ketersediaan dan akses ke Praktek Dokter', 0.0325841, 8),
(12, 'Ketersediaan dan akses ke Praktek Bidan', 0.0299338, 8),
(13, 'Ketersediaan dan akses ke Poskesdes atau Polindes', 0.0252111, 8),
(14, 'Ketersediaan dan akses ke pertokoan,minimarket,warung kelontong', 0.0196165, 9),
(15, 'Ketersediaan dan akses ke Pasar', 0.0179773, 9),
(16, 'Ketersediaan dan akses ke Rumah makan atau Kedai makan', 0.0152138, 9),
(17, 'Ketersediaan dan kondisi penerangan di jalan utama', 0.0188277, 10),
(18, 'Infrastruktur Energi', 0.0299481, 10),
(19, 'Infrastruktur air bersih dan sanitas', 0.030138, 11),
(20, 'Sumber air untuk mandi atau cuci', 0.0137127, 11),
(21, 'Fasilitas internet, pos, dan pengiriman barang', 0.0174274, 12),
(22, 'Lalu lintas dan kualitas jalan', 0.0422595, 13),
(23, 'Aksebilitas jalan', 0.0177129, 13),
(24, 'Ketersediaan dan oprasional angkutan umum', 0.0280166, 13),
(25, 'Waktu / jarak tempuh perkilometer transportasi kekantor camat', 0.0142172, 14),
(26, 'Biaya perkilometer transportasi ke kantor camat', 0.0264609, 14),
(28, 'Biaya perkilometer transportasi ke kantor bupati', 0.0351981, 14),
(30, 'Ketersediaan Lapangan Olahraga', 0.0432162, 16),
(31, 'Kelompok Kegiatan olahraga', 0.0213563, 16),
(40, 'Bahan bakar untuk memasak', 0.0421201, 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`id_desa`),
  ADD KEY `id_kecamatan` (`id_kecamatan`);

--
-- Indexes for table `dimensi`
--
ALTER TABLE `dimensi`
  ADD PRIMARY KEY (`id_dimensi`);

--
-- Indexes for table `indikator`
--
ALTER TABLE `indikator`
  ADD PRIMARY KEY (`id_indikator`),
  ADD KEY `id_dimensi` (`id_dimensi`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unik_data` (`tahun`,`id_variabel`,`id_desa`),
  ADD KEY `id_variabel` (`id_variabel`,`id_desa`),
  ADD KEY `id_desa` (`id_desa`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_desa` (`id_desa`),
  ADD KEY `id_kecamatan` (`id_kecamatan`);

--
-- Indexes for table `variabel`
--
ALTER TABLE `variabel`
  ADD PRIMARY KEY (`id_variabel`),
  ADD KEY `id_indikator` (`id_indikator`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `desa`
--
ALTER TABLE `desa`
  MODIFY `id_desa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dimensi`
--
ALTER TABLE `dimensi`
  MODIFY `id_dimensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `indikator`
--
ALTER TABLE `indikator`
  MODIFY `id_indikator` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `variabel`
--
ALTER TABLE `variabel`
  MODIFY `id_variabel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `desa`
--
ALTER TABLE `desa`
  ADD CONSTRAINT `desa_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`);

--
-- Constraints for table `indikator`
--
ALTER TABLE `indikator`
  ADD CONSTRAINT `indikator_ibfk_1` FOREIGN KEY (`id_dimensi`) REFERENCES `dimensi` (`id_dimensi`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_variabel`) REFERENCES `variabel` (`id_variabel`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_desa`) REFERENCES `desa` (`id_desa`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`id_desa`) REFERENCES `desa` (`id_desa`);

--
-- Constraints for table `variabel`
--
ALTER TABLE `variabel`
  ADD CONSTRAINT `variabel_ibfk_1` FOREIGN KEY (`id_indikator`) REFERENCES `indikator` (`id_indikator`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
