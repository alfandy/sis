
<div class="container-fluid"><br><br><br>

	<div class="alert alert-success" role="alert">
		<i class="fas fa-university"></i> 
		<?php echo $judul?>
	</div>

	


	<form method="post" action="<?php echo base_url('administrator/dashboard/inputan_tambah') ?>">
		<?php if (form_error('tahun')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('tahun'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Tahun</label>
			<input type="number" name="tahun" class="form-control"   placeholder="Masukan Tahun Penginputan " value="<?php echo set_value('tahun') ?>">
		</div>
		
		<?php if (form_error('id_variabel')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('id_variabel'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Variabel</label>
			<select class="form-control" name="id_variabel">
				<option value="">-- Pilih Variabel ---</option>
				<?php if (!empty($forenkey)): ?>
					<?php foreach ($forenkey as $value): ?>
						<option value="<?php echo $value->id_variabel ?>"><?php echo $value->nama ?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select>
		</div>

		<?php if (form_error('ketersediaan')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('ketersediaan'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Ketersediaan</label>
			<input type="number" name="ketersediaan" class="form-control"   placeholder="Input Ketersediaan" value="<?php echo set_value('ketersediaan') ?>">
		</div>

		<?php if (form_error('akses')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('akses'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Akses</label>
			<select class="form-control" name="akses">
				<option value="0">0</option>
				<option value="1">1</option>
				
			</select>
		</div>

		

		<button type="submit" class="btn btn-primary">Proses</button>
	</form>
	


</div>
