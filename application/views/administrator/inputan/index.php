<div class="container-fluid"><br><br><br>
	<div class="alert alert-success" role="alert">
		<i class="fas fa-university"></i> 
		<?php echo $judul?>
	</div>


	<?php echo $this->session->flashdata('pesan') ?>

	<a href="<?php echo base_url('administrator/dashboard/inputan_tambah') ?>"  class="btn btn-sm btn-primary mb-3"> Tambah <?php echo $tombol?></a>


	<table class="table table-bordered table-hover table-striped">
		<tr>
			
			<th width="1%">No </th>
			<th>Tahun </th>
			<th>Desa</th>
			<th>Variabel</th>
			<th>Pembobotan</th>
			<th>Ketersediaan</th>
			<th>Akses</th>



			
			<th colspan="3" width="10%">Aksi</th>
		</tr>


		<?php if ( !empty($data)): ?>
			<?php
			$no = 1;
			foreach ($data as $vr) : ?>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $vr->tahun ?></td>
					<td><?php echo $vr->desaName ?></td>
					<td><?php echo $vr->variabelName ?></td>
					<td><?php echo $vr->variabelPembobotan ?></td>
					<td><?php echo $vr->ketersediaan ?></td>
					<td><?php echo $vr->akses ?></td>
					
					<td width="15px" >
						<a href="<?php echo base_url('administrator/dashboard/inputan_edit/'.$vr->id) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
					</td>
					<td width="15px" >
						<a href="<?php echo base_url('administrator/dashboard/inputan_hapus/'.$vr->id) ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
					</td>
				</tr> 

			<?php endforeach; ?>
		<?php endif ?>

		
	</table>


</div>