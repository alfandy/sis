<div class="container-fluid"><br><br><br>
	<div class="alert alert-success" role="alert">
		<i class="fas fa-university"></i> 
		<?php echo $judul?>
	</div>


	<?php echo $this->session->flashdata('pesan') ?>

	<a href="<?php echo base_url('administrator/dashboard/user_tambah') ?>"  class="btn btn-sm btn-primary mb-3"> Tambah <?php echo $tombol?></a>


	<table class="table table-bordered table-hover table-striped">
		<tr>
			
			<th width="1%">No </th>
			<th>Nama </th>
			<th>Email </th>
			<th>Username </th>
			<th>Password </th>
			<th>Level </th>
			<th>Desa </th>
			<th>Kecamatan </th>
			
			<th colspan="3" width="10%">Aksi</th>
		</tr>


		<?php if ( !empty($data)): ?>
			<?php
			$no = 1;
			foreach ($data as $vr) : ?>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $vr->nama ?></td>
					<td><?php echo $vr->userEmail ?></td>
					<td><?php echo $vr->userUsername ?></td>
					<td><?php echo $vr->userPassword ?></td>
					<td><?php echo $vr->userLevel ?></td>
					<td><?php echo $vr->desaNama ?></td>
					<td><?php echo $vr->kecamatanNama ?></td>

					
					
					<td width="15px" >
						<a href="<?php echo base_url('administrator/dashboard/user_edit/'.$vr->id) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
					</td>
					<td width="15px" >
						<a href="<?php echo base_url('administrator/dashboard/user_hapus/'.$vr->id) ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
					</td>
				</tr> 

			<?php endforeach; ?>
		<?php endif ?>

		
	</table>


</div>