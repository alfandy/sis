
<div class="container-fluid"><br><br><br>

	<div class="alert alert-success" role="alert">
		<i class="fas fa-university"></i> 
		<?php echo $judul?>
	</div>

	


	<form method="post" action="<?php echo base_url('administrator/dashboard/user_tambah') ?>">
		<?php if (form_error('nama')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('nama'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Nama </label>
			<input type="text" name="nama" class="form-control" required  placeholder="Masukan Nama User Anda">
		</div>
		<?php if (form_error('email')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('email'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Email</label>
			<input type="text" name="email" class="form-control" required  placeholder="Masukan Email Anda">
		</div>
		<?php if (form_error('username')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('username'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Username</label>
			<input type="text" name="username" class="form-control" required  placeholder="Masukan Username Anda">
		</div>
		<?php if (form_error('password')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('password'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Password</label>
			<input type="text" name="password" class="form-control" required  placeholder="Masukan Password Anda">
		</div>
		<?php if (form_error('level')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('level'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Level</label>
			<select class="form-control" name="level">
				<!-- <option value="0">super admin</option>
				<option value="1">Kecamatan</option> -->
				<option value="2">Kepala Desa</option>
				<option value="3">Sekretaris</option>
				
			</select>
		</div>
		<?php if (form_error('id_desa')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('id_desa'); ?>
			</div>
		<?php endif ?>
		
		<div class="form-group">

			<label>Desa</label>
			<select class="form-control" name="id_desa" required>
				<option value="">
					--- Pilih Desa ---
				</option>
				<?php if (!empty($forenkey)): ?>
					<?php foreach ($forenkey as $value): ?>
						<option value="<?php echo $value->id_desa ?>"><?php echo $value->nama ?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select>
		</div>
		<!-- <?php if (form_error('id_kecamatan')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('id_kecamatan'); ?>
			</div>
		<?php endif ?> -->
		
		<!-- <div class="form-group">

			<label>Kecamatan</label>
			<select class="form-control" name="id_kecamatan">
				<option value="">-- Pilih Kecamatan ---</option>
				<?php if (!empty($forenkey2)): ?>
					<?php foreach ($forenkey2 as $value): ?>
						<option value="<?php echo $value->id_kecamatan ?>"><?php echo $value->nama ?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select>
		</div> -->

		<button type="submit" class="btn btn-primary">Proses</button>
	</form>
	


</div>

