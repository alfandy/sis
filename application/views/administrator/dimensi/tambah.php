
<div class="container-fluid"><br><br><br>

	<div class="alert alert-success" role="alert">
		<i class="fas fa-university"></i> 
		<?php echo $judul?>
	</div>

	


	<form method="post" action="<?php echo base_url('administrator/dashboard/dimenis_tambah') ?>">
		<?php if (form_error('nama')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('nama'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Nama Dimensi</label>
			<input type="text" name="nama" class="form-control"  placeholder="Masukan Nama Dimensi ">
		</div>

		<button type="submit" class="btn btn-primary">Proses</button>
	</form>
	


</div>
