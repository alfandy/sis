<div class="container-fluid"><br><br>
 
 <div class="alert alert-success" role="alert">
  <i class="fas fa-tachometer-alt"></i> 
   Dashboard
 </div>
<?php echo $this->session->flashdata('pesan') ?>
 <div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Selamat Datang..</h4>
  <p> Selamat Datang <strong><?php echo $username;?></strong> di Sistem Informasi Indeks Pembangunan Desa, Anda Login Sebagai <strong><?php echo $levelNama; ?></strong></p>

  <hr>
  <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModel">
  	<i class="fas fa-cogs"></i> Control Panel
  </button>
</div> 


<div class="modal fade" id="exampleModel" tabindex="-1" role="dialog" aria-labelledby="exampleModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		<div class="modal-headre">
		<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-cogs"></i> Control Panel</h5>
		<button type="button" class="close" data-dismis="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
			
		</div>
		<div class="modal-body">
		<div class="row">
			<div class="col-md-3 text-info text-center">
			<a href="<?php echo base_url() ?>"><p class="nav-link small text-info">Tentang Desa</p></a>
				<i class="fas fa-3x fa-info-circle"></i>
			</div>
			
				<div class="col-md-3 text-info text-center">
				<a href="<?php echo base_url() ?>"><p class="nav-link small text-info">Grafik IPD</p></a>
				<i class="fas fa-3x fa-calendar-alt"></i>
			</div>
			
				<div class="col-md-3 text-info text-center">
				<a href="<?php echo base_url() ?>"><p class="nav-link small text-info">Laporan IPD</p></a>
				<i class="fas fa-3x fa-file-alt"></i>
			</div>

				<div class="col-md-3 text-info text-center">
				<a href="<?php echo base_url() ?>"><p class="nav-link small text-info">Info Desa</p></a>
				<i class="fas fa-3x fa-bullhorn"></i>
			</div>
		</div><hr>
		<div class="row">
			<div class="col-md-3 text-info text-center">
			<a href="<?php echo base_url() ?>"><p class="nav-link small text-info">Input Nilai IPD</p></a>
				<i class="fas fa-3x fa-sort-numeric-down"></i>
			</div>
			
				<div class="col-md-3 text-info text-center">
				<a href="<?php echo base_url() ?>">Cetak Laporan IPD<p class="nav-link small text-info"></p></a>
				<i class="fas fa-3x fa-print"></i>
			</div>
			
				<div class="col-md-3 text-info text-center">
				<a href="<?php echo base_url() ?>"><p class="nav-link small text-info">Galery</p></a>
				<i class="fas fa-3x fa-images"></i>
			</div>

				<div class="col-md-3 text-info text-center">
				<a href="<?php echo base_url() ?>"><p class="nav-link small text-info">Kontak</p></a>
				<i class="fas fa-3x fa-id-card-alt"></i>
			</div>
			
					
		</div>

		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismis="modal"> Close</button>
			<button type="button" class="btn btn-primary">Save changes</button>
			
		</div>
			
		</div>

	</div>
	
</div>
</div>



