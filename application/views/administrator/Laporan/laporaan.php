<div class="container-fluid"><br><br><br>
	<div class="alert alert-success" role="alert">
		<i class="fas fa-university"></i> 
		<?php echo $judul?>
	</div>
	<button class="btn btn-sm btn-info mb-3" onclick="pirntData()"><i class="fas fa-plus fa-sm"></i>Print</button> <a href="<?php echo base_url('administrator/dashboard/laporan') ?>" class="btn btn-danger btn-sm mb-3">Kembali</a>
	<div id="printIni">
		
		<center class="mb-4">
		 	<legend class="mt-3"><strong>LAPORAN INDEKS PEMBANGUNAN DESA (IPD)</strong></legend>

		 	<table>
		 		<tr>
		 			<td><strong>Nama Desa</strong></td>
		 			<td>&nbsp;: <?php echo $namaDesa ?></td>
		 		</tr>
		 		<tr>
		 			<td><strong>Tahun</strong></td>
		 			<td>&nbsp;: <?php echo $tahun ?></td>
		 		</tr>



		 	</table>

		 </center>

		<table class="table table-bordered table-hover table-striped">
			<tr>
				
				<th width="1%">No </th>
				<th>Variabel</th>
				<th>Pembobotan</th>
				<th>Ketersediaan</th>
				<th>Akses</th>



			<?php 

				$Pembobotan = [];
				$ketersediaan = [];
				$akses = [];
			 ?>	

			<?php if ( !empty($data)): ?>
				<?php
				$no = 1;
				
				foreach ($data as $vr) : ?>
					<tr>
						<td><?php echo $no++ ?></td>
						<td><?php echo $vr->variabelName ?></td>
						<td><?php echo $vr->variabelPembobotan ?></td>
						<td><?php echo $vr->ketersediaan ?></td>
						<td><?php echo $vr->akses ?></td>
						
						
						<?php 
							$Pembobotan[] = $vr->variabelPembobotan;
							$ketersediaan[] = $vr->ketersediaan;
							$akses[] = $vr->akses;
						 ?>

				<?php endforeach; ?>
				
			<?php endif ?>
			<tr>
				<td colspan="2">
					Jumlah
				</td>
				<td><?php echo $pem = array_sum($Pembobotan) ?></td>
				<td><?php echo $ket = array_sum($ketersediaan) ?></td>
				<td><?php echo $aks = array_sum($akses) ?></td>
			</tr>


		</table>
		<?php 
	 		$total = (($ket + $aks) * $pem);
	 		$status = '';
			$color = '';
	 		if ($total > 75) {
	 			$status = 'Mandiri';
	 			$color = 'green';
	 		}
	 		if ($total >= 70 && $total <= 75) {
	 			$status = 'Berkembang';
	 			$color = 'gren';
	 		}
	 		if ($total < 70) {
	 			$status = 'Tertinggal';
	 			$color = 'red';
	 		}


	 	 ?>
		Indeks Pembangunan Desa : <?php echo number_format($total,2); ?>
		<p><b style="color: <?php echo $color?>;"><?php echo $status ?></b></p>
	</div>


</div>

<script type="text/javascript">
	function pirntData() {
		$("#printIni").print();
	}
</script>