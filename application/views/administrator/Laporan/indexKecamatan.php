
<div class="container-fluid"><br><br><br>

	<div class="alert alert-success" role="alert">
		<i class="fas fa-university"></i> 
		<?php echo $judul?>
	</div>

	


	<form method="post" action="<?php echo base_url('administrator/dashboard/laporan') ?>">
		
		<div class="form-group">

			<label>Tahun</label>
			<input type="number" name="tahun" class="form-control" required  placeholder="Masukan Tahun Laporan ">
		</div>
		<div class="form-group">

			<label>Desa</label>
			<select class="form-control" name="id_desa">
				<option value="">--- Pilih Desa ---</option>
				<?php if (!empty($dropdownDesa)): ?>
					<?php foreach ($dropdownDesa as $value): ?>
						<option value="<?php echo $value->id_desa ?>"><?php echo $value->nama ?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select>
		</div>
		<button type="submit" class="btn btn-primary">lihat</button>
	</form>
	


</div>
