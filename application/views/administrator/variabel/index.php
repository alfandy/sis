<div class="container-fluid"><br><br><br>
	<div class="alert alert-success" role="alert">
		<i class="fas fa-university"></i> 
		<?php echo $judul?>
	</div>


	<?php echo $this->session->flashdata('pesan') ?>

	<a href="<?php echo base_url('administrator/dashboard/variabel_tambah') ?>"  class="btn btn-sm btn-primary mb-3"> Tambah <?php echo $tombol?></a>


	<table class="table table-bordered table-hover table-striped">
		<tr>
			
			<th width="1%">No </th>
			<th>Nama </th>
			<th>Pembobotan </th>
			<th>Idikator </th>
			
			<th colspan="3" width="10%">Aksi</th>
		</tr>


		<?php if ( !empty($data)): ?>
			<?php
			$no = 1;
			foreach ($data as $vr) : ?>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $vr->nama ?></td>
					<td><?php echo $vr->pembobotan ?></td>

					<td><?php echo $vr->indikatorName ?></td>
					
					<td width="15px" >
						<a href="<?php echo base_url('administrator/dashboard/variabel_edit/'.$vr->id_variabel) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
					</td>
					<td width="15px" >
						<a href="<?php echo base_url('administrator/dashboard/variabel_hapus/'.$vr->id_variabel) ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
					</td>
				</tr> 

			<?php endforeach; ?>
		<?php endif ?>

		
	</table>


</div>