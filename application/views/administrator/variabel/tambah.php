
<div class="container-fluid"><br><br><br>

	<div class="alert alert-success" role="alert">
		<i class="fas fa-university"></i> 
		<?php echo $judul?>
	</div>

	


	<form method="post" action="<?php echo base_url('administrator/dashboard/variabel_tambah') ?>">
		<?php if (form_error('nama')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('nama'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Nama Variabel</label>
			<input type="text" name="nama" class="form-control" required  placeholder="Masukan Nama Variabel ">
		</div>
		<?php if (form_error('pembobotan')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('pembobotan'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Pembobotan</label>
			<input type="float" name="pembobotan" class="form-control" required  placeholder="Masukan Nilai Pembobotan ">
		</div>
		<?php if (form_error('id_indikator')): ?>
			<div class="alert alert-danger" role="alert">
				<?php echo form_error('id_indikator'); ?>
			</div>
		<?php endif ?>
		<div class="form-group">

			<label>Indikator</label>
			<select class="form-control" name="id_indikator">
				<option value="">-- Pilih Indikator ---</option>
				<?php if (!empty($forenkey)): ?>
					<?php foreach ($forenkey as $value): ?>
						<option value="<?php echo $value->id_indikator ?>"><?php echo $value->nama ?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select>
		</div>

		<button type="submit" class="btn btn-primary">Proses</button>
	</form>
	


</div>
