<?php  
    $levelNo = $this->session->userdata['level'];
 ?>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/administrator/dashboard">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-university"></i> 
        </div>
        <div class="sidebar-brand-text mx-1"> SISFO-IPD </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('administrator/dashboard') ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <?php if ($levelNo != '0'): ?>
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Data IPD </span>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header"> Data IPD :</h6>
              <?php if ($levelNo == '3'): ?>
                <a class="collapse-item" href="<?php echo base_url('administrator/dashboard/inputan') ?>">Inputan IPD</a>
              <?php endif ?>
              <a class="collapse-item" href="<?php echo base_url('administrator/dashboard/laporan') ?>">Laporan</a>
            </div>
          </div>
        </li>
      <?php endif ?>

      
      <!-- Nav Item - Utilities Collapse Menu -->
      <?php if ($levelNo == '1' OR $levelNo == '0'): ?>
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Pengaturan</span>
          </a>
          <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Pengaturan :</h6>
              <?php if ($levelNo == '1' OR $levelNo == '0'): ?>
                <a class="collapse-item" href="<?php echo base_url('administrator/dashboard/user') ?>">User</a>
              <?php endif ?>
              <?php if ($levelNo == '0'): ?>
                <a class="collapse-item" href="<?php echo base_url('administrator/dashboard/dimensi') ?>">Dimensi</a>
                <a class="collapse-item" href="<?php echo base_url('administrator/dashboard/indikator') ?>">Indikator</a>
                <a class="collapse-item" href="<?php echo base_url('administrator/dashboard/variabel') ?>">Variabel</a>
                <a class="collapse-item" href="utilities-border.html">Kecamatan</a>
                <a class="collapse-item" href="utilities-border.html">Desa</a>
              <?php endif ?>
            </div>
          </div>
        </li>
      <?php endif ?>
      

      

     
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('administrator/auth/logout') ?>">
          <i class="fas fa-fw  fa-chart-area"></i>
          <span>Logout</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>