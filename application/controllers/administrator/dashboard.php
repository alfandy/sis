<?php

/**
 * 
 */
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		if (!isset($this->session->userdata['username'])) {
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
				Anda Belum login!
				<button type="button" class="close" data-dismis="alert" aria-label="Close">
				</button>
				</div> ');	
			redirect('administrator/auth');
			
		}
	}
	
	public function index()
	{
		$data = $this->user_model->ambil_data($this->session->userdata['username']);
		switch ($data->level) {
			case '0':
			$levelNama = 'Super Admin';
			break;
			case '1':
			$levelNama = 'kecamantan';
			break;

			case '2':
			$levelNama = 'Kepala Desa';
			break;

			case '3':
			$levelNama = 'Sekertaris';
			break;
			
			default:
			$levelNama = 'Tidak Ada Level';
			break;
		}
		$data = array(
			'username' => $data->username,
			'level'  => $data->level,
			'levelNama'  => $levelNama,
		);
		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/dashboard',$data);
		$this->load->view('templates_administrator/footer');

	}


	// master dimensi
	public function dimensi()
	{
		$this->cekAkses('0');
		$datas = $this->db->get('dimensi')->result();
		$data = array(
			'judul' => 'Data Master Dimensi',
			'tombol' => 'Dimensi',
			'data' => $datas
		); 

		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/dimensi/index',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function dimenis_tambah()
	{
		$this->cekAkses('0');
		$data = array(
			'judul' => ' Form Inputan Data Master Dimensi',
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('nama','Nama Dimensi','required',[
				'required' => 'Nama Dimensi Tidak Boleh Kosong'
			]);
			if ($this->form_validation->run() == FALSE) {
					// return false;
			}else{
				$this->db->insert('dimensi' ,array(
					'nama' => $this->input->post('nama')
				));
				redirect('administrator/dashboard/dimensi');
			}	


			// $this->db->insert('my_ta')
		}
		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/dimensi/tambah',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function dimenis_edit($id)
	{
		$this->cekAkses('0');
		$where = $this->db->where('id_dimensi',$id);
		$datas = $where->get('dimensi')->row();
		$data = array(
			'judul' => 'Form Edit Data Master Dimensi',
			'data' => $datas
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('nama','Nama Dimensi','required',[
				'required' => 'Nama Dimensi Tidak Boleh Kosong'
			]);
			if ($this->form_validation->run() == FALSE) {
					// return false;
			}else{
				$this->db->where('id_dimensi',$id)->update('dimensi' ,array(
					'nama' => $this->input->post('nama')
				));
				
				redirect('administrator/dashboard/dimensi');
			}	


			// $this->db->insert('my_ta')
		}
		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/dimensi/edit',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function dimenis_hapus($id)
	{	
		$this->cekAkses('0');
		$status = $this->db->delete('dimensi' , array(
			'id_dimensi' => $id
		));
		if (!$status) {

			// var_dump('error');
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
				Maaf Data Ini Tidak dapat Di hapus
				<button type="button" class="close" data-dismis="alert" aria-label="Close">

				</button>
				</div> ');
		}
		redirect('administrator/dashboard/dimensi');
	}
	// end master dimensi

	// master indikator
	public function indikator()
	{
		$this->cekAkses('0');
		$datas = $this->db->select('indikator.* , dimensi.nama as dimensiNama')
			->from('indikator')
			->join('dimensi','dimensi.id_dimensi=indikator.id_dimensi','left')
			->get()
			->result();
		$data = array(
			'judul' => 'Data Master indikator',
			'tombol' => 'Indikator',
			'data' => $datas
		); 

		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/indikator/index',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function indikator_tambah()
	{
		$this->cekAkses('0');
		$forenkey = $this->db->get('dimensi')->result();
		$data = array(
			'judul' => ' Form Inputan Data Master Indikator',
			'forenkey' => $forenkey
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('nama','Nama indikator','required',[
				'required' => 'Nama Dimensi Tidak Boleh Kosong'
			]);
			$this->form_validation->set_rules('id_dimensi','Nama Dimensi','required',[
				'required' => 'Dimensi Harus Di pilih'
			]);
			if ($this->form_validation->run() == FALSE) {
					// return false;
			}else{
				$this->db->insert('indikator' ,array(
					'nama' => $this->input->post('nama'),
					'id_dimensi' => $this->input->post('id_dimensi')
				));
				redirect('administrator/dashboard/indikator');
			}	


			// $this->db->insert('my_ta')
		}
		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/indikator/tambah',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function indikator_edit($id)
	{
		$this->cekAkses('0');
		$forenkey = $this->db->get('dimensi')->result();
		$where = $this->db->where('id_indikator',$id);
		$datas = $where->get('indikator')->row();
		$data = array(
			'judul' => 'Form Edit Data Master Indikator',
			'data' => $datas,
			'forenkey' => $forenkey
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('nama','Nama indikator','required',[
				'required' => 'Nama Indikator Tidak Boleh Kosong'
			]);
			$this->form_validation->set_rules('id_dimensi','Nama Dimensi','required',[
				'required' => 'Dimensi Harus Di pilih'
			]);
			if ($this->form_validation->run() == FALSE) {
					// return false;
			}else{
				$this->db->where('id_indikator',$id)->update('indikator' ,array(
					'nama' => $this->input->post('nama'),
					'id_dimensi' => $this->input->post('id_dimensi')
				));
				
				redirect('administrator/dashboard/indikator');
			}	


			// $this->db->insert('my_ta')
		}
		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/indikator/edit',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function indikator_hapus($id)
	{
		$this->cekAkses('0');
		$status = $this->db->delete('indikator' , array(
			'id_indikator' => $id
		));
		if (!$status) {

			// var_dump('error');
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
				Maaf Data Ini Tidak dapat Di hapus
				<button type="button" class="close" data-dismis="alert" aria-label="Close">

				</button>
				</div> ');
		}
		redirect('administrator/dashboard/indikator');
	}

	// end master indikator

	// master variabel
	public function variabel()
	{
		$this->cekAkses('0');

		$datas = $this->db->select('variabel.* , indikator.nama as indikatorName')
			->from('variabel')
			->join('indikator','variabel.id_indikator=indikator.id_indikator','left')
			->get()
			->result();
		$data = array(
			'judul' => 'Data Master Variabel',
			'tombol' => 'Variabel',
			'data' => $datas
		); 

		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/variabel/index',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function variabel_tambah()
	{
		$this->cekAkses('0');
		$forenkey = $this->db->get('indikator')->result();
		$data = array(
			'judul' => ' Form Inputan Data Master Variabel',
			'forenkey' => $forenkey
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('nama','Nama Variabel','required',[
				'required' => 'Nama Variabel Tidak Boleh Kosong'
			]);
			$this->form_validation->set_rules('id_indikator','Nama Indikator','required',[
				'required' => 'Indikator Harus Di pilih'
			]);
			if ($this->form_validation->run() == FALSE) {
					// return false;
			}else{
				$this->db->insert('variabel' ,array(
					'nama' => $this->input->post('nama'),
					'id_indikator' => $this->input->post('id_indikator')
				));
				redirect('administrator/dashboard/variabel');
			}	


			// $this->db->insert('my_ta')
		}
		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/variabel/tambah',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function variabel_hapus($id)
	{
		$status = $this->db->delete('variabel' , array(
			'id_variabel' => $id
		));
		if (!$status) {

			// var_dump('error');
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
				Maaf Data Ini Tidak dapat Di hapus
				<button type="button" class="close" data-dismis="alert" aria-label="Close">

				</button>
				</div> ');
		}
		redirect('administrator/dashboard/variabel');
		// https://oauth2:G8HiRL3DZyknntzR2aQH@gitlab.com/alfandy/sis.git
	}


public function inputan()
	{
		$this->cekAkses('3');
		$datas = $this->db->select('transaksi.* , variabel.nama as variabelName ,variabel.pembobotan as variabelPembobotan, desa.nama as desaName')
			->from('transaksi')
			->join('variabel','variabel.id_variabel=transaksi.id_variabel','left')
			->join('desa','desa.id_desa=transaksi.id_desa','left')
			->where('transaksi.id_desa' , $this->session->userdata['id_desa'])
			->order_by('transaksi.tahun','desc')
			->order_by('transaksi.id_variabel','desc')
			->get()
			->result();
		$data = array(
			'judul' => 'Data Master indikator',
			'tombol' => 'Transaksi',
			'data' => $datas
		); 

		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/inputan/index',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function inputan_tambah()
	{

		$this->cekAkses('3');

		$forenkey = $this->db->get('variabel')->result();
		$data = array(
			'judul' => ' Form Inputan Data IPD',
			'forenkey' => $forenkey
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('tahun','Tahun','required',[
				'required' => 'Tahun Tidak Boleh Kosong'
			]);
			$this->form_validation->set_rules('id_variabel','Nama Variabel','required',[
				'required' => 'Variabel Harus Di pilih'
			]);
			$this->form_validation->set_rules('ketersediaan','Ketersediaan ','required',[
				'required' => 'Ketersediaan Harus Di pilih'
			]);
			$this->form_validation->set_rules('akses','Akses','required',[
				'required' => 'Akses Harus Di pilih'
			]);
			if ($this->form_validation->run() == FALSE) {
					// return false;
			}else{

				$this->db->insert('transaksi' ,array(
					'tahun' => $this->input->post('tahun'),
					'ketersediaan' => $this->input->post('ketersediaan'),
					'akses' => $this->input->post('akses'),
					'id_variabel' => $this->input->post('id_variabel'),
					'id_desa' => $this->session->userdata['id_desa'],
					'id_user' => $this->session->userdata['id'],
				));
				redirect('administrator/dashboard/inputan');
			}	
				// $this->db->insert('my_ta')
		}
		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/inputan/tambah',$data);
		$this->load->view('templates_administrator/footer');
	}

	public function inputan_hapus($id)
	{
		$status = $this->db->delete('transaksi' , array(
			'id' => $id
		));
		if (!$status) {

			// var_dump('error');
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
				Maaf Data Ini Tidak dapat Di hapus
				<button type="button" class="close" data-dismis="alert" aria-label="Close">

				</button>
				</div> ');
		}
		redirect('administrator/dashboard/inputan');
		// https://oauth2:G8HiRL3DZyknntzR2aQH@gitlab.com/alfandy/sis.git
	}

	public function laporan()
	{
		if ($this->session->userdata('level') == '3' OR $this->session->userdata('level') == '2') {
			// ini unutk level sekretaris
			$data = array(
				'judul' => 'Form Masuk Halaman Indeks Pembangunan Desa',
			);
			if ($this->input->post()) {

				$datas = $this->db->select('transaksi.* , variabel.nama as variabelName ,variabel.pembobotan as variabelPembobotan, desa.nama as desaName')
					->from('transaksi')
					->join('variabel','variabel.id_variabel=transaksi.id_variabel','left')
					->join('desa','desa.id_desa=transaksi.id_desa','left')
					->where('transaksi.id_desa' , $this->session->userdata['id_desa'])
					->where('transaksi.tahun' , $this->input->post('tahun'))
					->get()
					->result();
				$namaDesa = $this->db->where('id_desa' ,  $this->session->userdata['id_desa'])->get('desa')->row();
				$data = array(
					'judul' => 'Laporan Indeks Pembangunan Desa(IPD)',
					'tombol' => 'Transaksi',
					'data' => $datas,
					'tahun' => $this->input->post('tahun'),
					'namaDesa' => $namaDesa->nama
				);

				$this->load->view('templates_administrator/header');
				$this->load->view('templates_administrator/sidebar');
				$this->load->view('administrator/Laporan/laporaan',$data);
				$this->load->view('templates_administrator/footer');
				
			}else{

				$this->load->view('templates_administrator/header');
				$this->load->view('templates_administrator/sidebar');
				$this->load->view('administrator/laporan/index',$data);
				$this->load->view('templates_administrator/footer');
			}
		}
		if ($this->session->userdata('level') == '1') {

			$this->laporanKecamatan();
		}
	
	}

	public function laporanKecamatan()
	{
		$forenkey = $this->db->where('id_kecamatan' , $this->session->userdata['id_kecamatan'])->get('desa')->result();
		$data = array(
				'judul' => 'Form Masuk Halaman Indeks Pembangunan Desa',
				'dropdownDesa' => $forenkey
			);
			if ($this->input->post()) {

				$datas = $this->db->select('transaksi.* , variabel.nama as variabelName ,variabel.pembobotan as variabelPembobotan, desa.nama as desaName')
					->from('transaksi')
					->join('variabel','variabel.id_variabel=transaksi.id_variabel','left')
					->join('desa','desa.id_desa=transaksi.id_desa','left')
					->where('transaksi.id_desa' , $this->input->post('id_desa'))
					->where('transaksi.tahun' , $this->input->post('tahun'))
					->get()
					->result();
				$namaDesa = $this->db->where('id_desa' ,  $this->input->post('id_desa'))->get('desa')->row();
				$data = array(
					'judul' => 'Laporan Indeks Pembangunan Desa(IPD)',
					'tombol' => 'Transaksi',
					'data' => $datas,
					'tahun' => $this->input->post('tahun'),
					'namaDesa' => $namaDesa->nama
				);

				$this->load->view('templates_administrator/header');
				$this->load->view('templates_administrator/sidebar');
				$this->load->view('administrator/Laporan/laporaan',$data);
				$this->load->view('templates_administrator/footer');
				
			}else{

				$this->load->view('templates_administrator/header');
				$this->load->view('templates_administrator/sidebar');
				$this->load->view('administrator/laporan/indexKecamatan',$data);
				$this->load->view('templates_administrator/footer');
			}
	}

	public function user()
	{
		if ($this->session->userdata['level'] == '1') {
			
			$datas = $this->db->select('user.* , user.nama as userName , user.email as userEmail , user.username as userUsername , user.password as userPassword ,  user.level as userLevel , desa.nama as desaNama, kecamatan.nama as kecamatanNama')
				->from('user')
				->join('desa','desa.id_desa=user.id_desa','left')
				->join('kecamatan','kecamatan.id_kecamatan=user.id_kecamatan','left')
				->where('user.id_kecamatan' , $this->session->userdata['id_kecamatan'])
				->where('level !=' , '1')
				->get()
				->result();
				$namaDesa = $this->db->where('id_desa' , $this->input->post('id_desa'))->get('desa')->row();
				$namaKecamatan = $this->db->where('id_kecamatan' , $this->input->post('id_kecamatan'))->get('desa')->row();
				$data = array(
				'judul' => 'Data Master User',
				'tombol' => 'User',
				'data' => $datas
			); 
		}
		if ($this->session->userdata['level'] == '0'){
			$datas = $this->db->select('user.* , user.nama as userName , user.email as userEmail , user.username as userUsername , user.password as userPassword ,  user.level as userLevel , desa.nama as desaNama, kecamatan.nama as kecamatanNama')
				->from('user')
				->join('desa','desa.id_desa=user.id_desa','left')
				->join('kecamatan','kecamatan.id_kecamatan=user.id_kecamatan','left')
				->where('level' , '1')
				->or_where('level' , '0')
				->get()
				->result();
				$namaDesa = $this->db->where('id_desa' , $this->input->post('id_desa'))->get('desa')->row();
				$namaKecamatan = $this->db->where('id_kecamatan' , $this->input->post('id_kecamatan'))->get('desa')->row();
				$data = array(
				'judul' => 'Data Master User',
				'tombol' => 'User',
				'data' => $datas
			); 
		}

		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/user/index',$data);
		$this->load->view('templates_administrator/footer');
	}

	
	public function user_tambah()
	{
		if ($this->session->userdata['level'] == '1') {
			# code...
			$forenkey = $this->db->where('id_kecamatan' , $this->session->userdata['id_kecamatan'])->get('desa')->result();
			// $dropdownKecamatan = $this->db->get('kecamatan')->result();
			$data = array(
				'judul' => ' Form Inputan Data User',
				'forenkey' => $forenkey,
				// 'forenkey2' => $dropdownKecamatan,
			);
			if ($this->input->post()) {
				$this->form_validation->set_rules('nama','Nama','required',[
					'required' => 'Nama Tidak Boleh Kosong'
				]);
				$this->form_validation->set_rules('email','Email','required',[
					'required' => 'Email Harus Di isi'
				]);
				$this->form_validation->set_rules('username','Username','required',[
					'required' => 'Username Harus Di isi'
				]);
				$this->form_validation->set_rules('password','Password','required',[
					'required' => 'Password Harus Di isi'
				]);
				$this->form_validation->set_rules('level','level','required',[
					'required' => 'Level Harus Di pilih'
				]);
				$this->form_validation->set_rules('id_desa','Nama Desa','required',[
					'required' => ' Desa Harus Di pilih'
				]);
				// $this->form_validation->set_rules('id_kecamatan','Nama Kecamatan','required',[
				// 	'required' => 'Id Kecamatan Harus Di pilih'
				// ]);

				if ($this->form_validation->run() == FALSE) {
						// return false;
				}else{
					$this->db->insert('user' ,array(
						'nama' => $this->input->post('nama'),
						'email' => $this->input->post('email'),
						'username' => $this->input->post('username'),
						'password' => md5($this->input->post('password')),
						'level' => $this->input->post('level'),
						'id_desa' => $this->input->post('id_desa'),
						'id_kecamatan' => $this->session->userdata['id_kecamatan'],
					));
					redirect('administrator/dashboard/user');
				}	
					// $this->db->insert('my_ta')
			}
			$this->load->view('templates_administrator/header');
			$this->load->view('templates_administrator/sidebar');
			$this->load->view('administrator/user/tambah',$data);
			$this->load->view('templates_administrator/footer');
		}
		if ($this->session->userdata['level'] == '0') {
			$this->user_tambah_admin();
		}
	}
	public function user_tambah_admin()
	{
		// $forenkey = $this->db->where('id_kecamatan' , $this->session->userdata['id_kecamatan'])->get('desa')->result();
		$dropdownKecamatan = $this->db->get('kecamatan')->result();
		$data = array(
			'judul' => ' Form Inputan Data User',
			// 'forenkey' => $forenkey,
			'forenkey2' => $dropdownKecamatan,
		);
		if ($this->input->post()) {
			$this->form_validation->set_rules('nama','Nama','required',[
				'required' => 'Nama Tidak Boleh Kosong'
			]);
			$this->form_validation->set_rules('email','Email','required',[
				'required' => 'Email Harus Di isi'
			]);
			$this->form_validation->set_rules('username','Username','required',[
				'required' => 'Username Harus Di isi'
			]);
			$this->form_validation->set_rules('password','Password','required',[
				'required' => 'Password Harus Di isi'
			]);
			$this->form_validation->set_rules('level','level','required',[
				'required' => 'Level Harus Di pilih'
			]);
			// $this->form_validation->set_rules('id_desa','Nama Desa','required',[
			// 	'required' => ' Desa Harus Di pilih'
			// ]);
			$this->form_validation->set_rules('id_kecamatan','Nama Kecamatan','required',[
				'required' => 'Id Kecamatan Harus Di pilih'
			]);

			if ($this->form_validation->run() == FALSE) {
					// return false;
			}else{
				$this->db->insert('user' ,array(
					'nama' => $this->input->post('nama'),
					'email' => $this->input->post('email'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('password')),
					'level' => $this->input->post('level'),
					// 'id_desa' => $this->input->post('id_desa'),
					'id_kecamatan' => $this->input->post('id_kecamatan'),
				));
				redirect('administrator/dashboard/user');
			}	
				// $this->db->insert('my_ta')
		}
		$this->load->view('templates_administrator/header');
		$this->load->view('templates_administrator/sidebar');
		$this->load->view('administrator/user/tambah_admin',$data);
		$this->load->view('templates_administrator/footer');
	}


	public function cekAkses($level)
	{
		if ($this->session->userdata['level'] != $level) {
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
				Maaf  Anda Tidak Memiliki Akses Kesini.
				<button type="button" class="close" data-dismis="alert" aria-label="Close">

				</button>
				</div> ');
			redirect('administrator/dashboard');
		}
	}
}